# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gst-python-0.10.22-r2.exheres-0', which is:
#     Copyright 2009 Sterling X. Winter <sterling.winter@gmail.com>

require python [ blacklist=2 multibuild=false ] meson

SUMMARY="GStreamer Python bindings (complementing the gobject-introspection based bindings)"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0
        gnome-bindings/pygobject:3[>=3.8][python_abis:*(-)?]
        media-libs/gstreamer:1.0[>=${PV}][gobject-introspection]
        media-plugins/gst-plugins-base:1.0[>=${PV}][gobject-introspection]
"

src_prepare() {
    meson_src_prepare

    # Use prefixed pkg-config
    edo sed -e "s/pkg-config/$(exhost --tool-prefix)&/g" \
            -i testsuite/meson.build
}

src_configure() {
    local meson_params=(
        -Dpygi-overrides-dir=$(python_get_sitedir)/gi/overrides/

        -Dplugin=enabled
        -Dtests=enabled
    )

    exmeson "${meson_params[@]}"
}

