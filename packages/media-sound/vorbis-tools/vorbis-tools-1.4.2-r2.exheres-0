# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Tools for the Ogg Vorbis audio format"
DESCRIPTION="
vorbis-tools contains oggenc (an encoder), ogg123 (a playback tool), ogginfo
(displays ogg information), oggdec (decodes ogg files), vcut (ogg file
splitter), and vorbiscomment (ogg comment editor).
"
HOMEPAGE="https://xiph.org/vorbis/"
DOWNLOADS="https://downloads.xiph.org/releases/vorbis/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    flac
    ogg123 [[ description = [ Ogg Vorbis audio player ] ]]
    speex  [[
        description = [ Add support for the speex audio codec in the Ogg123 audio player ]
        requires = [ ogg123 ]
    ]]
"

# TODO: support for media-libs/libkate::media-unofficial
# oggenc needs either kate or flac
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libogg[>=1.0]
        media-libs/libvorbis[>=1.3.0]
        media-libs/opusfile[>=0.2]
        flac? ( media-libs/flac:=[>=1.1.3] )
        ogg123? (
            media-libs/libao[>=1.0.0]
            net-misc/curl
        )
        speex? ( media-libs/speex )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-{ogg{dec,info},vcut,vorbiscomment}
    --disable-static
    --without-kate
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "flac oggenc"
    ogg123
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    flac
    speex
)

DEFAULT_SRC_COMPILE_PARAMS=(
    # Prefix AR for musl libc
    AR=$(exhost --tool-prefix)ar
)

