# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2013 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PN}${PV:0:1}-${PV}"

require cmake

SUMMARY="The Simple DirectMedia Layer library"
HOMEPAGE="https://libsdl.org"
DOWNLOADS="${HOMEPAGE}/release/${MY_PNV}.tar.gz"

LICENCES="ZLIB"
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    X
    ibus [[ description = [ Adds support for ibus input method ] ]]
    jack
    libsamplerate
    libunwind [[ description = [ Use libunwind to get stack traces ] ]]
    pipewire [[ description = [ Adds support for PipeWire sound server ] ]]
    pulseaudio
    sndio [[ description = [ Adds support for sound output through sndio (OpenBSD sound API, also ported to Linux) ] ]]
    wayland

    libunwind? (
        ( providers: libunwind llvm-libunwind ) [[ number-selected = exactly-one ]]
    )

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# CMake configure failure when tests are enabled, last checked: 2.24.0
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libglvnd[X?]
        sys-apps/dbus
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa[X?][wayland?]
        ibus? ( inputmethods/ibus )
        jack? ( media-sound/jack-audio-connection-kit )
        libsamplerate? ( media-libs/libsamplerate )
        libunwind? (
            providers:libunwind? ( dev-libs/libunwind )
            providers:llvm-libunwind? ( sys-libs/llvm-libunwind )
        )
        pipewire? ( media/pipewire[>=0.3.20] )
        pulseaudio? ( media-sound/pulseaudio )
        sndio? ( sys-sound/sndio )
        wayland? (
            sys-libs/libdecor
            sys-libs/wayland[>=1.18]
            x11-libs/libxkbcommon[>=0.5.0]
        )
        X? (
            x11-libs/libX11
            x11-libs/libXcursor
            x11-libs/libXext
            x11-libs/libXfixes
            x11-libs/libXi
            x11-libs/libXt
            x11-libs/libXrender
            x11-libs/libXrandr
            x11-libs/libXScrnSaver
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

CMAKE_SOURCE=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( WhatsNew )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSDL2_DISABLE_INSTALL:BOOL=FALSE
    -DSDL2_DISABLE_SDL2MAIN:BOOL=FALSE
    -DSDL2_DISABLE_UNINSTALL:BOOL=FALSE
    -DSDL_ALSA:BOOL=TRUE
    -DSDL_ARTS:BOOL=FALSE
    -DSDL_ARTS_SHARED:BOOL=FALSE
    -DSDL_ASAN:BOOL=FALSE
    -DSDL_BACKGROUNDING_SIGNAL:STRING=OFF
    -DSDL_CCACHE:BOOL=FALSE
    -DSDL_DBUS:BOOL=TRUE
    -DSDL_ESD:BOOL=FALSE
    -DSDL_FOREGROUNDING_SIGNAL:STRING=OFF
    -DSDL_FUSIONSOUND:BOOL=FALSE
    -DSDL_FUSIONSOUND_SHARED:BOOL=FALSE
    -DSDL_HIDAPI:BOOL=TRUE
    -DSDL_HIDAPI_JOYSTICK:BOOL=TRUE
    -DSDL_HIDAPI_LIBUSB:BOOL=FALSE
    -DSDL_INSTALL_TESTS:BOOL=FALSE
    -DSDL_KMSDRM:BOOL=TRUE
    -DSDL_KMSDRM_SHARED:BOOL=TRUE
    -DSDL_NAS:BOOL=FALSE
    -DSDL_OPENGL:BOOL=TRUE
    -DSDL_OPENGLES:BOOL=TRUE
    -DSDL_OSS:BOOL=TRUE
    -DSDL_RPATH:BOOL=FALSE
    -DSDL_RPI:BOOL=FALSE
    -DSDL_SHARED:BOOL=TRUE
    -DSDL_STATIC:BOOL=FALSE
    -DSDL_SYSTEM_ICONV:BOOL=TRUE
    -DSDL_VIRTUAL_JOYSTICK:BOOL=TRUE
    -DSDL_VULKAN:BOOL=TRUE
    -DSDL_WASAPI:BOOL=FALSE
    -DSDL_WAYLAND_QT_TOUCH:BOOL=FALSE
    -DSDL_WERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'X SDL_X11'
    'X SDL_X11_SHARED'
    'X SDL_X11_XCURSOR'
    'X SDL_X11_XDBE'
    'X SDL_X11_XFIXES'
    'X SDL_X11_XINPUT'
    'X SDL_X11_XRANDR'
    'X SDL_X11_XSCRNSAVER'
    'X SDL_X11_XSHAPE'
    'ibus SDL_IBUS'
    'jack SDL_JACK'
    'jack SDL_JACK_SHARED'
    'libsamplerate SDL_LIBSAMPLERATE'
    'libsamplerate SDL_LIBSAMPLERATE_SHARED'
    'pipewire SDL_PIPEWIRE'
    'pipewire SDL_PIPEWIRE_SHARED'
    'pulseaudio SDL_PULSEAUDIO'
    'pulseaudio SDL_PULSEAUDIO_SHARED'
    'sndio SDL_SNDIO'
    'sndio SDL_SNDIO_SHARED'
    'wayland SDL_WAYLAND'
    'wayland SDL_WAYLAND_LIBDECOR'
    'wayland SDL_WAYLAND_LIBDECOR_SHARED'
    'wayland SDL_WAYLAND_SHARED'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DSDL_TEST:BOOL=TRUE -DSDL_TEST:BOOL=FALSE'
    '-DSDL_TESTS:BOOL=TRUE -DSDL_TESTS:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    if ! option libunwind; then
        edo sed -e 's/libunwind.h//' -i "${CMAKE_SOURCE}"/CMakeLists.txt
    fi
}

