# Copyright 2008 Richard Brown
# Copyright 2015 Thomas G. Anderson
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libmad-0.15.1b.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 1.12 1.11 1.10 ] ]

SUMMARY="MPEG audio decoder library"
HOMEPAGE="http://www.underbit.com/products/mad/"
DOWNLOADS="mirror://sourceforge/mad/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="debug"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-cflags.patch )

src_configure() {
    local myconf=( '--enable-accuracy' )
    case $(exhost --target) in
        x86_64-*|aarch64-*)
            myconf+=( '--enable-fpm=64bit' )
        ;;
        i686-*)
            myconf+=( '--enable-fpm=intel' )
        ;;
        arm*)
            myconf+=( '--enable-fpm=arm' )
        ;;
        *)
            die "Your compilation target platform is not supported by ${PNV}"
        ;;
    esac

    econf \
        "${myconf[@]}" \
        $(option_enable debug debugging)
}

src_install() {
    default

    dodir /usr/$(exhost --target)/lib/pkgconfig
    insinto /usr/$(exhost --target)/lib/pkgconfig
    doins "${FILES}"/mad.pc
    edo sed -i -e "s:^prefix.*:prefix=/usr/$(exhost --target):" \
        "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/mad.pc
}

