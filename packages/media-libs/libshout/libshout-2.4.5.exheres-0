# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for streaming audio to icecast or shoutcast-compatible servers"
DESCRIPTION="
A library to allow applications to easily communicate and broadcast to an Icecast or Shoutcast
streaming media server.
"
HOMEPAGE="http://www.icecast.org"
DOWNLOADS="http://downloads.us.xiph.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    speex
    theora
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libogg
        media-libs/libvorbis
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        speex? ( media-libs/speex )
        theora? ( media-libs/libtheora )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-pkgconfig
    --disable-examples
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    speex
    theora
)

DEFAULT_SRC_INSTALL_PARAMS=( docdir="/usr/share/doc/${PNVR}" )

