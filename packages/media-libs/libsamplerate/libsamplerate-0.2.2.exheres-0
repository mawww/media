# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=libsndfile release=${PV} suffix=tar.xz ]

SUMMARY="Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for audio"
DESCRIPTION="
Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for audio.
One example of where such a thing would be useful is converting audio from the
CD sample rate of 44.1kHz to the 48kHz sample rate used by DAT players.

SRC is capable of arbitrary and time varying conversions ; from downsampling by
a factor of 256 to upsampling by the same factor. Arbitrary in this case means
that the ratio of input and output sample rates can be an irrational number.
The conversion ratio can also vary with time for speeding up and slowing down effects.

SRC provides a small set of converters to allow quality to be traded off
against computation cost. The current best converter provides a signal-to-noise
ratio of 145dB with -3dB passband extending from DC to 96% of the theoretical
best bandwidth for a given pair of input and output sample rates.
"
HOMEPAGE+=" https://libsndfile.github.io/libsamplerate/"

UPSTREAM_CHANGELOG="https://github.com/libsndfile/libsamplerate/blob/master/ChangeLog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="https://libsndfile.github.io/libsamplerate/api.html [[ lang = en ]]"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="examples"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    test:
        media-libs/libsndfile[>=1.0.6]
        sci-libs/fftw[>=0.15.0]
        sys-sound/alsa-lib
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --disable-werror
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-alsa --disable-alsa'
    '--enable-fftw --disable-fftw'
    '--enable-sndfile --disable-sndfile'
)

src_install() {
    default

    emagicdocs

    insinto /usr/share/doc/${PNVR}/html
    doins docs/*.{css,md,png}

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins examples/*.c
    fi
}

