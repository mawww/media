# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=gphoto suffix=tar.xz ] udev-rules

export_exlib_phases src_prepare src_install

SUMMARY="Portable library that supports numerous digital cameras"
HOMEPAGE+="proj/${PN}"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: cs da de es eu fr hu it ja nl pl ru sv uk vi zh_CN )
"

# doc: once https://github.com/gphoto/libgphoto2/issues/294 is fixed
# internal-doc: fails to build, last checked: 2.5.28
# doc? (
#    app-doc/doxygen[dot]
#    dev-doc/gtk-doc
#)
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.1]
        virtual/pkg-config[>=0.9.0]
    build+run:
        group/plugdev
        dev-libs/libusb:1
        dev-libs/libxml2:2.0
        media-libs/gd[>=2.0]
        media-libs/libexif[>=0.6.13]
        net-misc/curl[>=7.1]
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    run:
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

#DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
#    'doc docs'
#    'doc internal-docs'
#)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    udevscriptdir=${UDEVDIR}
    --enable-nls
    --disable-docs
    --disable-internal-docs
    --with-gd
    --with-jpeg
    --with-libxml2
    --without-hal
    --without-ws232
)

libgphoto2_src_prepare() {
    export PKG_CONFIG=$(type -P $PKG_CONFIG)

    default
}

libgphoto2_src_install() {
    default

    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${IMAGE}/usr/$(exhost --target)/lib"
    export CAMLIBS="${IMAGE}/usr/$(exhost --target)/lib/${PN}/${PV}"

    dodir ${UDEVRULESDIR}
    edo "${IMAGE}"/usr/$(exhost --target)/lib/${PN}/print-camera-list udev-rules version 201 group plugdev mode 0666 > "${IMAGE}"/${UDEVRULESDIR}/40-${PN}.rules

    dodir ${UDEVHWDBDIR}
    edo "${IMAGE}"/usr/$(exhost --target)/lib/${PN}/print-camera-list hwdb > "${IMAGE}"/${UDEVHWDBDIR}/20-${PN}.hwdb
}

