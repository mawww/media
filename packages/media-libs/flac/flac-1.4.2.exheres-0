# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=xiph suffix=tar.xz release=${PV} ]
require toolchain-funcs
require alternatives

SUMMARY="The Free Lossless Audio Codec"
HOMEPAGE+=" https://xiph.org/${PN}"

LICENCES="Xiph || ( GPL-2 GPL-3 )"
SLOT="10.12"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="ogg"

# All tests are slow, so use only src_test_expensive
RESTRICT="test"

DEPENDENCIES="
    build+run:
        ogg? ( media-libs/libogg )
    run:
        !media-libs/flac:0[<1.3.4-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

flac-cc_define_enable() {
    cc-has-defined ${1} && echo --enable-${2} || echo --disable-${2}
}

src_configure() {
    local myconf=()

    myconf+=(
        --enable-cpplibs
        # They are relatively small
        --enable-programs
        --enable-stack-smash-protection
        --disable-doxygen-docs
        --disable-examples
        --with-ogg-libraries=/dummy
        $(option_enable ogg)
        # x86
        $(flac-cc_define_enable __AVX__ avx)
    )

    econf "${myconf[@]}"
}


src_test_expensive() {
    emake check
}

src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives=(
        /usr/${host}/bin/flac                flac-${SLOT}
        /usr/${host}/bin/metaflac            metaflac-${SLOT}
        /usr/${host}/include/FLAC            FLAC-${SLOT}
        /usr/${host}/include/FLAC++          FLAC++-${SLOT}
        /usr/${host}/lib/libFLAC.la          libFLAC-${SLOT}.la
        /usr/${host}/lib/libFLAC.so          libFLAC-${SLOT}.so
        /usr/${host}/lib/libFLAC++.la        libFLAC++-${SLOT}.la
        /usr/${host}/lib/libFLAC++.so        libFLAC++-${SLOT}.so
        /usr/${host}/lib/libFLAC.la          libFLAC-${SLOT}.la
        /usr/${host}/lib/pkgconfig/flac.pc   flac-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/flac++.pc flac++-${SLOT}.pc
    )

    other_alternatives=(
        /usr/share/aclocal/libFLAC.m4   libFLAC-${SLOT}.m4
        /usr/share/aclocal/libFLAC++.m4 libFLAC++-${SLOT}.m4
        /usr/share/man/man1/flac.1      flac-${SLOT}.1
        /usr/share/man/man1/metaflac.1  metaflac-${SLOT}.1
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

