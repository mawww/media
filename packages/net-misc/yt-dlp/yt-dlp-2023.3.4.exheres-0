# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'youtube-dl-2021.06.06.exheres-0', which is:
#     Copyright 2010-2015 Johannes Nixdorf <mixi@shadowice.org>

require bash-completion zsh-completion
require pypi setup-py [ import=setuptools blacklist=2 has_bin=true ]

SUMMARY="Command-line program to download videos from YouTube.com and many other other video platforms"

LICENCES="Unlicense"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

# they need internet access
RESTRICT="test"

DEPENDENCIES="
    run:
        dev-python/certifi[python_abis:*(-)?]
        dev-python/brotlicffi[python_abis:*(-)?]
        dev-python/mutagen[python_abis:*(-)?]
        dev-python/pycryptodomex[python_abis:*(-)?]
        dev-python/websockets[python_abis:*(-)?]
    suggestion:
        media/ffmpeg [[
            description = [ Many extractors have FFmpeg as a requirement ]
        ]]
"

BASH_COMPLETIONS=( completions/bash/${PN} )
ZSH_COMPLETIONS=( completions/zsh/_${PN} )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # do not install bash/fish/zsh completions
    # do not install README.txt to /usr/share/doc/${PN}
    edo sed \
        -e '/completions\/bash/d' \
        -e '/completions\/fish/d' \
        -e '/completions\/zsh/d' \
        -e '/README.txt/d' \
        -i setup.py
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    bash-completion_src_install
    zsh-completion_src_install
}

